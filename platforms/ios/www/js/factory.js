angular.module('starter.factories', [])


.factory('loadMainJson', function($http,$rootScope) {
	var data = [];

	return {
		loadMain: function(user,recipent){
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
		
			return $http.get($rootScope.Host+'/mainJson.php').then(function(resp){
				data = resp.data;
				//return users;
				//console.log ('main json: ',data);
				return data;
				});
		},
		
		
		loadCategories: function(user,recipent){
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
	
			return $http.get($rootScope.Host+'/get_categories.php').then(function(resp){
			data = resp.data;
			//return users;
			//console.log ('main json: ',data);
			return data;
			});
		}

		
		/*
		getUser: function(id){
			for(i=0;i<users.length;i++){
				if(users[i].id == id){
					return users[i];
				}
			}
			return null;
		}
		*/
	}
})
